#!/bin/bash

set -e
RELDIR=$(dirname "${0}")
BASEDIR=$(realpath "${RELDIR}/..")
if [ -z "$1" ]; then
    echo "Usage: $0 [project_root]"
    echo "     add devise auth support"
    exit 1
fi
TARGET=$(realpath "$1")

set -x
cd "${TARGET}"
# We need docker compose up to be running
docker-compose up -d

# Add gems
if ! grep '^gem .devise' "${TARGET}/Gemfile"; then
    docker-compose exec app bundle add devise omniauth-saml
fi
cp -r "${BASEDIR}/devise/rails_root/"* "${TARGET}"
echo 'devise.yaml' >>"${TARGET}/.gitignore"
echo 'Generating a devise.yaml file for you:'
docker-compose exec app bundle exec rake saml:generate
set +x
echo 'Would you like to generate / modify your user model for devise? (default y) [y|n]'
echo '  (hint: if you want to do this later just look at the template in this repo)'
read -r CONFIRM
if [ "$CONFIRM" == "y" ] || [ "$CONFIRM" == "" ]; then
    set -x
    docker-compose exec app bundle exec rails g devise user
#    cp -r "${BASEDIR}/devise/optional/app" "${TARGET}"
fi
set +x
cat <<EOF
Devise added. If you generated a migration file, take a look and edit it before you
run the migration.

You may wish to add 'lib/generators/active_record/templates/*' to the AllCops -> Exclude section of your .rubocop.yml

**IMPORTANT** !

You must ensure you add the routes to make all this work. Please ensure you have
the following in routes.rb

  # Callbacks - where a browser returns to the site with extra session data
  # inserted from the SSO server by devise + omniauth
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }

  # Used by omniauth
  get '/auth/:provider/callback', to: 'sessions#create'
  devise_scope :user do
    get 'sign_in', to: 'sessions#new', as: :new_user_session
    get 'sign_out', to: 'sessions#destroy', as: :destroy_user_session
  end

EOF
