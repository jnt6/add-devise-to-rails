# frozen_string_literal: true

require 'bundler/setup'
require 'openssl'
require 'json'
require 'yaml'
# This is not intended to load rails, so rails environment shouldn't be needed.
# rubocop:disable Metrics/BlockLength, Rails/RakeEnvironment
namespace :saml do
  BASE_DATA = {
    'name' => 'sso_auth',
    'idp_entity_id' => 'https://shib.oit.duke.edu/shibboleth-idp',
    'idp_sso_target_url' => 'https://shib.oit.duke.edu/idp/profile/SAML2/Redirect/SSO',
    'idp_slo_target_url' => 'https://shib.oit.duke.edu/cgi-bin/logout.pl',
    'uid_attribute' => 'urn:mace:duke.edu:idms:unique-id',
    'attribute_statements' => {
      'name' => ['urn:oid:2.16.840.1.113730.3.1.241'],
      'email' => ['urn:oid:0.9.2342.19200300.100.1.3'],
      'last_name' => ['urn:oid:2.5.4.4'],
      'first_name' => ['urn:oid:2.5.4.42'],
      'phone' => ['urn:oid:2.5.4.20']
    }
  }.freeze

  task :generate do
    desc 'Generate Files for omniauth using Saml'
    STDOUT.puts <<~MSG
      Generating a saml config file for omniauth / devise

      Input the base URL of this app for the shib redirect
         e.g.: 'https://foo.oit.duke.edu' or 'http://127.0.0.1:3000'
           (ensure you include the port, if it is non-standard)
    MSG
    url_base = STDIN.gets.strip
    STDOUT.puts 'Cert name - i.e. a unique string to identify this deployment'
    issuer = STDIN.gets.strip
    fname = 'devise.yaml'
    STDOUT.puts "Path of the generated file? (default #{fname})"
    fnameinput = STDIN.gets.strip
    fname = fnameinput unless fnameinput.empty?
    key = generate_key
    cert = generate_cert(key, issuer)
    acs_url = "#{url_base}/users/auth/sso_auth/callback"
    settings = {
      'issuer' => issuer,
      'idp_cert' => idp_cert,
      'private_key' => key.to_s,
      'certificate' => cert.to_s,
      'assertion_consumer_service_url' => acs_url
    }.merge(BASE_DATA).to_yaml
    File.write(fname, settings)
    cert_str = cert.to_s.split("\n")
    cert_str.delete_at(-1)
    cert_str.delete_at(0)
    STDOUT.puts <<~MSG
      Done! Please register using the following information at:
        https://shib.oit.duke.edu

      Entity ID:    #{issuer}
      ACS URL:      #{acs_url}
      Released Attributes: duDukeID (at least)
      Certificate:

      #{cert_str.join("\n")}

    MSG
  end

  def idp_cert
    <<~IDPCERT
      MIIEWjCCA0KgAwIBAgIJAP1rB/FjRgy6MA0GCSqGSIb3DQEBBQUAMHsxCzAJBgNV
      BAYTAlVTMRcwFQYDVQQIEw5Ob3J0aCBDYXJvbGluYTEPMA0GA1UEBxMGRHVyaGFt
      MRgwFgYDVQQKEw9EdWtlIFVuaXZlcnNpdHkxDDAKBgNVBAsTA09JVDEaMBgGA1UE
      AxMRc2hpYi5vaXQuZHVrZS5lZHUwHhcNMTAwOTA5MTI0NDU1WhcNMjgwOTA0MTI0
      NDU1WjB7MQswCQYDVQQGEwJVUzEXMBUGA1UECBMOTm9ydGggQ2Fyb2xpbmExDzAN
      BgNVBAcTBkR1cmhhbTEYMBYGA1UEChMPRHVrZSBVbml2ZXJzaXR5MQwwCgYDVQQL
      EwNPSVQxGjAYBgNVBAMTEXNoaWIub2l0LmR1a2UuZWR1MIIBIjANBgkqhkiG9w0B
      AQEFAAOCAQ8AMIIBCgKCAQEAt+hnl6gSRi0Y8VuNl6PCPYejj7VfVs/y8bRa5zAY
      RHwb75+vBSs2j1yeUcSore9Ba5Ni7v947V34afRMGRPOqr4TEDZxU+1Bg0zAvSrR
      n4Y8B+zyJuhtOpmOZzTwE9o/Oc+CB4kYV/K0woKZdcoxHJm8TbqBqdxU4fFYUlNU
      o4Dr5jRdCSr9MHBOqGWXtQMg16qYNB7StNk4twY29FNnpZwkVTfsE76uVsRMkG8i
      6/RiHpXZ/ioOOqndptbEGdsOIE3ivAJOZdvYwnDe5NnTH06P01HsxH3OOnYqhuG2
      J6qdhqoelGeHRG+jfl8YkYXCcKQvja2tJ5G+6iqSN7DP6QIDAQABo4HgMIHdMB0G
      A1UdDgQWBBQHYXwB6otkfyMOmUI59j8823hFRDCBrQYDVR0jBIGlMIGigBQHYXwB
      6otkfyMOmUI59j8823hFRKF/pH0wezELMAkGA1UEBhMCVVMxFzAVBgNVBAgTDk5v
      cnRoIENhcm9saW5hMQ8wDQYDVQQHEwZEdXJoYW0xGDAWBgNVBAoTD0R1a2UgVW5p
      dmVyc2l0eTEMMAoGA1UECxMDT0lUMRowGAYDVQQDExFzaGliLm9pdC5kdWtlLmVk
      dYIJAP1rB/FjRgy6MAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEFBQADggEBAG7q
      wJpiSLJbx2gj/cGDYeuBW/CeRGNghjQ/mb076P3WXsRNPAimcXulSUbQkS6eDH4t
      Ifvsa0jf4FRsEOwH/x8354/0wyv4RwuavX25kjpmoFn3O+eKokyzsc7/Q2gsm0mv
      V8XQo+5b+4we8AFYlAVp26nLeIqAiJM8xZJ9yHuzVL1O4yxIWIKECWHLqY5+1nas
      XNiLURrHhsK5pZUPLuhzJFgZuJT62TtnrjJXlrRhJ389VSkh6R64C6ncjNkg6/Cu
      tA6SX0infqNRyPRNJK+bnQd1yOP4++tjD/lAPE+5tiD/waI3fArt43ZE/qp7pYMS
      9TEfyQ5QpfRYAUFWXBc=
    IDPCERT
  end

  def generate_saml_settings
    BASE_DATA
  end

  def generate_key
    OpenSSL::PKey::RSA.new(2048)
  end

  def generate_cert key, issuer
    public_key = key.public_key

    # generate subject line of cert
    subject = generate_cert_subject(issuer)

    cert = OpenSSL::X509::Certificate.new

    # TODO: this line breaks when https:// is added for CN
    cert.subject = cert.issuer = OpenSSL::X509::Name.parse(subject)
    cert.not_before = Time.now.utc
    cert.not_after = Time.now.utc + 365 * 24 * 60 * 60 * 5
    cert.public_key = public_key
    cert.serial = 0x0
    cert.version = 2

    ef = OpenSSL::X509::ExtensionFactory.new
    ef.subject_certificate = cert
    ef.issuer_certificate = cert
    cert.extensions = [
      ef.create_extension('basicConstraints', 'CA:TRUE', true),
      ef.create_extension('subjectKeyIdentifier', 'hash')
    ]
    cert.add_extension ef.create_extension('authorityKeyIdentifier',
                                           'keyid:always,issuer:always')

    cert.sign key, OpenSSL::Digest.new('SHA256')
  end

  def generate_cert_subject issuer
    country = 'US'
    state = 'North Carolina'
    city = 'Durham'
    org = 'Duke University'
    unit = 'OIT'
    email = 'oit-automation@duke.edu'

    "/C=#{country}/ST=#{state}/L=#{city}/O=#{org}/OU=#{unit}/CN=#{issuer}/" \
      "emailAddress=#{email}"
  end
end
# rubocop:enable Metrics/BlockLength, Rails/RakeEnvironment
