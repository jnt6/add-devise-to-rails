class SessionsController < Devise::SessionsController
  # Required by devise. This controller either redirects or
  # displays links to the following actions.
  #
  # If you are globally requiring require_sso! or other actions that assume auth, make
  # sure you disable them on this controller explicitly
  #
  # skip_before_action :require_sso!, :authenticate
  #
  def new
    # Use the 'developer' provider instead when in the test env. You can auth with code
    # like this in a system test:
    # visit device_types_url
    # fill_in 'Name', with: 'Test user'
    # fill_in 'Email', with: 'test.user@duke.edu'
    # page.all('button')[0].click
    #
    # Your 'provider' will be 'developer' and  'provider_id' will be 'test.user@duke.edu'

    if ENV['USE_DUMMY_AUTH_PROVIDER'].present?
      redirect_to user_developer_omniauth_authorize_path
      return
    end
    redirect_to user_sso_auth_omniauth_authorize_path
  end

  def after_sign_out_path_for _provider
    after_sign_out_path
  end

  def after_sign_out
    if user_signed_in?
      redirect_to root_path, notice: 'You are signed in'
      return
    end
    render 'after_sign_out', layout: false
  end
end
