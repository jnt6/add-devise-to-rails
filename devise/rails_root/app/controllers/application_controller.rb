# frozen_string_literal: true

class ApplicationController < ActionController::Base
  private

  # Used by rails-combined_log to generate the username to log
  def current_username
    current_user&.netid
  end

  def denied
    render body: 'Access denied', status: :forbidden
  end

  # Required, used by devise
  def new_session_path _scope
    new_user_session_path
  end

  # Used in a before action to ensure users are logged in
  def require_sso!
    authenticate_user!
    denied if current_user.provider_id.blank?
  end
end
