# frozen_string_literal: true

class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  skip_before_action :verify_authenticity_token
  # If you are globally requiring require_sso!, make sure you disable
  # it on this controller explicitly
  # skip_before_action :require_sso!
  def developer
    sso_auth
  end

  def sso_auth
    # You need to implement the method below in your model (e.g. app/models/user.rb)
    @user = User.from_omniauth(request.env['omniauth.auth'])

    if @user.persisted?
      # this will throw if @user is not activated
      sign_in_and_redirect @user, event: :authentication
      set_flash_message(:notice, :success, kind: 'sso_auth') if is_navigational_format?
    else
      session['devise.auth_data'] = request.env['omniauth.auth']
    end
  end

  def failure
    redirect_to root_path
  end
end
