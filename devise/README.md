# Add devise

This repo and script help setup devise + saml auth. It's intended to be run against a brand new rails project from `rails new`, but if you run it over an existing project you can run a diff and integrate the changes it makes into your own app.

## Run ../bin/add-devise.sh [MYAPPDIR] which will do for you...

This script will do the following:

1. Add the devise and saml gems
1. Add a `sessions_controller.rb` with some required methods
1. Add an `application_controller.rb` with some requried methods
1. Add a `omniauth_callbacks_controller.rb` with some required methods
1. Add and run a `saml:generate` rake task to output a devise.yaml file ready to use with our IDP
1. Monkey patches the standard devise generator with defaults we want and (optionally) runs it to set up a user model.
    *you could implement the equivalent of the user model some other way, but this will store it in activerecord*

## Now you have to...
Note that you will need to **manually** perform the following steps:

1. **Unlike saml_camel, you'll need to persist any of the data from the saml response yourself** (i.e. ismemberof, email, etc). This means you may need to modify the model to track what you care about. The omniauth info hash includes [several standard attributes like name, email, etc](https://github.com/omniauth/omniauth/wiki/Auth-Hash-Schema#schema-10-and-later) - and you may wish to add them as proper DB fields. You also need to modify [this method in user.rb](https://git-internal.oit.duke.edu/oit-automation/rails-bootstrap-tools/-/blob/master/devise/optional/app/models/user.rb#L21) to populate that data on the model when the user logs in.
    1. *By default, all the raw info of the provider is stored in the raw_data json column. If you don't care about interop with other providers, you can hard code an expectation for the saml data you will find in that field. You can also use that field to figure out the saml attribute urn -> standard attribute name mapping*
1. Use the `before_action :require_sso!` method in the included application.rb file for any controllers that should require sso, or add it to your application controller if they all should. **if you set this as the default in application_controller.rb, you need to also skip_before_action :require_sso! in omniauth_callbacks_controller.rb and sessions_controller.rb**
1. Use the `current_user` devise helper method in controllers to access the AR model instance
1. Use the `user_signed_in?` method to see if a user is authenticated
1. Add the required routes for the devise methods to routes.rb. They should look like:
    ```ruby
      # Callbacks - where a browser returns to the site with extra session data
      # inserted from the SSO server by devise + omniauth
      devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }

      # Used by omniauth
      get '/auth/:provider/callback', to: 'sessions#create'
      devise_scope :user do
        get 'sign_in', to: 'sessions#new', as: :new_user_session
        delete 'sign_out', to: 'sessions#destroy', as: :destroy_user_session
        get 'after_sign_out', to: 'sessions#after_sign_out'
      end
    ```
1. Add a logout link, if you need it (such a link is included in this repo and you get it if you use webpacker - it's in the application.html.haml file for webpacker). The included controller and view demonstrate a logout that 1) logs the user out of the app (i.e. destroys its local session) 2) de-auths the current session with the shib IDP. Note that in the common case of requiring a login for **all** resources you **must** override the path for `after_sign_out` as this controller demonstrates (if you do not do so, the user will bounce back to `rails_root`, which will immediately initiate a new SAML login)
1. Perform the saml registration, as documented by the output of 'rake saml:generate' (which creates the devise.yaml file). **Make sure you ask for duDukeID in the registration if you use the default uid attribute**
1. Restart your app (since it has new initializers)
1. Run `rake saml:generate` every time you want to deploy a new saml auth site
1. **Insert a new devise.yaml into the deployment workflow for production sites**. It could come from vault or a mounted OKD secret, for example. If you use the vault_tool from this repo, [it has convenience methods to upload and retrieve devise yaml](https://git-internal.oit.duke.edu/oit-automation/rails-bootstrap-tools/-/blob/master/vault/rails_root/lib/vault_tool.rb#L36). You can use that helper method to upload your yaml file on your developer workstation, and then [the included devise initializer will use VaultTool to try to retrieve it](https://git-internal.oit.duke.edu/oit-automation/rails-bootstrap-tools/-/blob/master/devise/rails_root/config/initializers/devise.rb#L8) before falling back to a conf file

## Fun stuff to do next

1. Once you're sure you have all the integration working, you may wish to change your development instance over to the 'developer' method, so you don't have to keep logging in locally. Grep for `Rails.env.test` in the initializer ([here](https://git-internal.oit.duke.edu/oit-automation/rails-bootstrap-tools/-/blob/master/devise/rails_root/config/initializers/devise.rb#L5) and [here](https://git-internal.oit.duke.edu/oit-automation/rails-bootstrap-tools/-/blob/master/devise/rails_root/config/initializers/devise.rb#L305)), the [model](https://git-internal.oit.duke.edu/oit-automation/rails-bootstrap-tools/-/blob/master/devise/optional/app/models/user.rb#L7), and the [sessions_controller](https://git-internal.oit.duke.edu/oit-automation/rails-bootstrap-tools/-/blob/master/devise/rails_root/app/controllers/sessions_controller.rb#L18) - and add the `Rails.env.development?` to the conditionals
1. In your testing, you can either go through a full login workflow, or stub the auth methods. They'd look like these, respectively:
    ```ruby
      # Just stub any session to be the admin user
      def stub_current_user user = users(:admin)
        ApplicationController.any_instance.stubs(:require_sso!).returns(true)
        ApplicationController.any_instance.stubs(:current_user).returns(user)
      end
    ```
    ```ruby
      # Walk through an actual auth workflow, and look up a user with the
      # provider id matching the 'email' field
      def system_test_log_in_as user = users(:admin)
          fill_in 'Name', with: user.name
          fill_in 'Email', with: user.provider_id
          page.all('button')[0].click
      end
    ```
