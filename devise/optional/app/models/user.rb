# A user model for devise
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  # devise :database_authenticatable, :registerable,
  #        :recoverable, :rememberable, :validatable
  if ENV['USE_DUMMY_AUTH_PROVIDER'].present?
    devise :omniauthable, omniauth_providers: [:developer]
  else
    devise :omniauthable, omniauth_providers: [:sso_auth]
  end
  # Implements last login data in user table
  devise :trackable
  # Session expiry
  devise :timeoutable, timeout_in: 6.hours

  def access?
    in_access_group?
  end

  def admin?
    in_admin_group?
  end

  def sso_groups
    raw_info&.dig('urn:oid:1.3.6.1.4.1.5923.1.5.1.1') || []
  end

  def in_access_group?
    sso_groups.include? ENV['ACCESS_GROUP']
  end

  def in_admin_group?
    sso_groups.include? ENV['ADMIN_GROUP']
  end

  def to_s
    name.presence || netid.presence || provider_id
  end

  def self.from_omniauth auth_data
    # The user will match the provider and provider_id provided by the SSO mechanism
    user = where(provider: auth_data.provider, provider_id: auth_data.uid)
           .first_or_initialize

    # The attributes from auth_data.info are generic omniauth attributes, and can be
    # relied upon with all supported auth providers. If you wish to be site
    # agnostic, rely only upon these for core functionality.
    #
    # The raw_info attributes, including the 'netid' concept, aren't universal,
    # so they are not available in 'info'. Since we use netid as a more human
    # friendly unique identifier, we explicitly persist it here. Remember that
    # this will be 'nil' for any other auth provider (including 'developer')
    # unless you take specific actions to populate it.

    user.raw_info = auth_data[:extra][:raw_info].to_h
    user.assign_attributes(
      name: auth_data.info.name,
      email: auth_data.info.email,
      netid: user.raw_info['urn:oid:1.3.6.1.4.1.5923.1.1.1.6']&.first
    )
    user.save! if user.changed?
    user
  end
end
